var GameBoardController = (function () {
    var instance = {};
    instance.init = function() {
        GameBoard.init();
        GameBoardView.init();
    }
    instance.insert = function(piece){
        GameBoard.insert(piece);
        GameBoardView.insert(piece);
        this.flipColors(piece);
    }
    instance.checkCollision = function(x, y){
        if(x > constants.WIDTH || x < 0){
            return false;
        }
        if(y > constants.HEIGHT || y < 0){
            return false;
        }
        return GameBoard.get(x, y) == -1;
    }
    instance.checkGameOver = function(x, y) {
        if(x == constants.START_X && y == constants.START_Y 
            && !this.checkCollision(x, y + 1)) {
            return true;
        }
        return false;
    }
    instance.flipColors = function(piece) {

        // left logic
        var left = 0;
        for(var i = 0; i < piece.x; i++) {
            if(GameBoard.get(piece.x - (i + 1), piece.y) == -1) {
                break;
            }
            if(GameBoard.get(piece.x - (i + 1), piece.y) == piece.color) {
                left = i;
                break;
            }
        }
        while(left > 0) {
            GameBoard.setPiece(piece.x - left, piece.y, piece.color);
            GameBoardView.setPiece(piece.x - left, piece.y, piece.color);
            left--;
        }

        // right logic
        var right = 0;
        for(var i = 0; i < constants.WIDTH - piece.x; i++) {
            if(GameBoard.get(piece.x + (i + 1), piece.y) == -1) {
                break;
            }
            if(GameBoard.get(piece.x + (i + 1), piece.y) == piece.color) {
                right = i;
                break;
            }
        }
        while(right > 0) {
            GameBoard.setPiece(piece.x + right, piece.y, piece.color);
            GameBoardView.setPiece(piece.x + right, piece.y, piece.color);
            right--;
        }

        // above logic
        var above = 0;
        for(var i = 0; i < piece.y; i++) {
            if(GameBoard.get(piece.x, piece.y - (i + 1)) == -1) {
                break;
            }
            if(GameBoard.get(piece.x, piece.y - (i + 1)) == piece.color) {
                above = i;
                break;
            }
        }
        while(above > 0) {
            GameBoard.setPiece(piece.x, piece.y - above, piece.color);
            GameBoardView.setPiece(piece.x, piece.y - above, piece.color);
            above--;
        }

        // below logic
        var below = 0;
        for(var i = 0; i < constants.HEIGHT - piece.y; i++) {
            if(GameBoard.get(piece.x, piece.y + (i + 1)) == -1) {
                break;
            }
            if(GameBoard.get(piece.x, piece.y + (i + 1)) == piece.color) {
                below = i;
                break;
            }
        }
        while(below > 0) {
            GameBoard.setPiece(piece.x, piece.y + below, piece.color);
            GameBoardView.setPiece(piece.x, piece.y + below, piece.color);
            below--;
        }
    }

    return instance;
})();
