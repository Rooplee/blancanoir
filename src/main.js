'use strict';
// controls the loading of the game, move outside FB calls to run locally
function loadDefaultGame() {
		var assets = [
		"img/bg.png",
		"img/animals.json",
	];

	var loader = PIXI.loader;
	loader.add(assets);

	// listen to the progress event
	loader.on('progress',function (loader,res) {
		//console.log("progress: " + loader.progress);
		FBInstant.setLoadingProgress(loader.progress);
	})
	// listen to the complete event, it will be fired when everything is loaded
	loader.on('complete',function (loader,res) {

		var windowWidth = window.innerWidth;
	    var windowHeight = window.innerHeight;
	    if(windowWidth > windowHeight){
	            windowWidth = windowHeight / 1.8;
	    }
			setup(windowWidth, windowHeight);
	});

	// start loading
	loader.load();
}

let images;
//This `setup` function will run when the images have loaded
function setup(width, height) {
    images = new Images();
    images.init();
	var bg = new PIXI.Sprite(PIXI.loader.resources["img/bg.png"].texture);
	//this part resizes the canvas but keeps ratio the same
    renderer.view.style.width = width + "px";
    renderer.view.style.height = height + "px";
    renderer.resize(width,height);
    constants.SIZE = width / 6;
	constants.SCALE = constants.SIZE / 64;
	constants.HEIGHT = Math.floor(height / constants.SIZE);

	app.stage.addChild(bg);
    app.stage.addChild(gameContainer);
	bg.interactive = true;
	bg.on('pointerdown', mouse.onClick);
	bg.on('pointerup', mouse.onRelease);
	//bg.on('pointerout', mouse.onRelease);
	bg.on('pointermove', mouse.onMove);
	bg.on('dragstart', mouse.onDrag);
	window.addEventListener('pointerdown',  mouse.onClick, true);
	window.addEventListener('pointerup',  mouse.onRelease, true);
	window.addEventListener('pointerout',  mouse.onRelease, true);
	window.addEventListener('pointermove',  mouse.onMove, true);
	window.addEventListener('dragstart',  mouse.onDrag, true);
	window.addEventListener('keydown', mouse.onKeyDown);
	Game.init();
    renderer.render(app.stage);
    app.ticker.add(delta => Game.loop(delta));
};
