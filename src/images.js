// holds the references for the sprites in the game.
function Images() {
	let id;
	let black;
	let white;

	this.init = function() {
		id = PIXI.loader.resources["img/animals.json"].textures;
		black = id["hedgehog.png"];
		white = id["cat.png"];
	};

	this.getImage = function(c) {
		switch(c) {
			case color.WHITE:
				return white;
			case color.BLACK:
				return black;
		}
	};
}

