var currentpiece = (function () {
	var instance = {};
	var currentPieceImage;
    instance.init = function() {
        currentPieceImage = new PIXI.Sprite(images.getImage(color.BLACK));
        currentPieceImage.scale.x = constants.SCALE;
        currentPieceImage.scale.y = constants.SCALE;
        currentPieceImage.position.set(0, 0);
        this.x = 0;
        this.y = 0;
        this.reset();
    }
	instance.image = function() {
		return currentPieceImage;
	}
	instance.reset = function() {
	    this.x = constants.START_X;
        this.y = constants.START_Y;
        this.color = Math.floor((Math.random() * NUMBER_COLORS));
		currentPieceImage.texture = images.getImage(this.color);
		this.update();
	}
	instance.update = function() {
		currentPieceImage.position.set(constants.SIZE * this.x, constants.SIZE * this.y);
        renderer.render(app.stage);
	}
    return instance;
})();
