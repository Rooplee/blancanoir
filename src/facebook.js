// holds the references for the sprites in the game.
var Facebook = (function() {
  var instance = {};

  instance.init = function() {
    FBInstant.initializeAsync()
    .then(function() {
      var locale = FBInstant.getLocale(); // 'en_US'
      var platform = FBInstant.getPlatform(); // 'IOS'
      var sdkVersion = FBInstant.getSDKVersion(); // '3.0'
      //console.log("locale: " + locale + " platform: " + platform + " sdkVersion: " + sdkVersion);

      // Start loading game assets here
      Facebook.startGameAsync();
    });
  }

  instance.startGameAsync = function() {
      var assets = [
    "img/bg.png",
    "img/animals.json",
    ];

    var loader = PIXI.loader;
    loader.add(assets);

    // listen to the progress event
    loader.on('progress',function (loader,res) {
      //console.log("progress: " + loader.progress);
      FBInstant.setLoadingProgress(loader.progress);
    })
    // listen to the complete event, it will be fired when everything is loaded
    loader.on('complete',function (loader,res) {
      FBInstant.startGameAsync()
      .then(function() {
        // Retrieving context and player information can only be done
        // once startGameAsync() resolves
        var contextId = FBInstant.context.getID();
        var contextType = FBInstant.context.getType();

        var playerName = FBInstant.player.getName();
        var playerPic = FBInstant.player.getPhoto();
        var playerID = FBInstant.player.getID();

        //console.log("contextId: " + contextId + " contextType: " + contextType + " playerName: " + playerName
          //+ " playerPic: " + playerPic + " playerID: " + playerID);

        // Once startGameAsync() resolves it also means the loading view has 
        // been removed and the user can see the game viewport

        var windowWidth = window.innerWidth;
        var windowHeight = window.innerHeight;
        if(windowWidth > windowHeight){
                windowWidth = windowHeight / 1.8;
        }
        setup(windowWidth, windowHeight);
      });
    });

    // start loading
    loader.load();
  }
  return instance;
})();

