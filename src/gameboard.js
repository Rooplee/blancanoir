var GameBoard = (function () {
	var instance = {};
	var gameboard = [];
	instance.init = function() {
		for(var i = 0; i <= constants.WIDTH; i++){
			gameboard[i] = [];
			for(var j = 0; j < constants.HEIGHT; j++) {
				gameboard[i][j] = -1;
			}
		}
	}
	instance.get = function(x, y) {
		return gameboard[x][y];
	}
	instance.insert = function(piece) {
		gameboard[piece.x][piece.y] = piece.color;
	}
	instance.setPiece = function(x, y, color) {
		gameboard[x][y] = color;
	}
	return instance;
})();
