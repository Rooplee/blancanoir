function Piece(piece) {
	var image;
	this.image = new PIXI.Sprite(images.getImage(piece.color));
	this.image.scale.x = constants.SCALE;
    this.image.scale.y = constants.SCALE;
    this.image.position.set(constants.SIZE * piece.x, constants.SIZE * piece.y);
	this.update = function(x, y) {
		this.image.position.set(constants.SIZE * x, constants.SIZE * y);
	}
	this.setImage = function(color) {
		this.image.texture = images.getImage(color);
	}
}
