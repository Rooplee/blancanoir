// This class handles mouse interactions based on the state of the game.
var mouse = (function () {
	var instance = {};
	var drag = false;

	instance.onClick = function(event) {
		this.drag = true;
		switch(Game.getState()) {
			case states.GAME:
			Game.onClick(event);
			break;
			default:
			break;
		}
	}

	instance.onMove = function(event) {
		if(this.drag) {
			switch(Game.getState()) {
				case states.GAME:
				Game.onMove(event);
				break;
				default:
				break;
			}
		}
	}

	instance.onDrag = function(event) {
	}

	instance.onRelease = function(event) {
		this.drag = false;
		switch(Game.getState()) {
			case states.GAME:
			Game.onRelease(event);
			break;
			default:
			break;
		}
	}
	instance.onKeyDown = function(event) {
		switch(Game.getState()){
			case states.GAME:
			Game.onKeyDown(event);
			break;
			default:
			break;
		}
	}
	return instance;
})();
