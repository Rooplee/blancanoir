// enums
var constants = {
       SIZE : 60,
       WIDTH : 6,
       HEIGHT : 10,
       SCALE : 1, // 64 px
       START_X : ((6 / 2) - 1),
       START_Y : 0,
}
var states = {
	NONE : 0,
	GAME : 1,
	MENU : 2,
}
var color = {
	WHITE : 0,
	BLACK  : 1,
}

// constants
const NUMBER_COLORS = 2;
const TICKER = 60;
const DEGREES_90 = 1.571; //radians

// texture cache for the image map
let TextureCache = PIXI.utils.TextureCache;
