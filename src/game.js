// Handles state interactions from the mouse
var Game = (function() {
		var instance = {};
		var tick = 0;
		var position = 0;
		var deltaY = 0;
		var deltaX = 0;
		var state = states.NONE;

		instance.getState = function() {
			return state;
		}

		instance.init = function() {
		currentpiece.init();
		GameBoardController.init();
		state = states.GAME;
		gameContainer.addChild(currentpiece.image());
		}

		instance.loop = function(delta) {
			if(state == states.GAME) {
				tick += delta;
				if(tick > TICKER){
					this.update();
				}
			}
		}

		instance.onClick = function(event) {
			this.deltaY = event.data.global.y;
			this.deltaX = event.data.global.x;
		}

		instance.onMove = function(event) {
			var position = event.data.global.x;
			if(event.data.global.x - this.deltaX > constants.SIZE
					&& currentpiece.x < constants.WIDTH - 1
					&& GameBoardController.checkCollision(currentpiece.x + 1, currentpiece.y)){
				currentpiece.x += 1;
				currentpiece.update();
				this.deltaX = event.data.global.x;
			} else if(event.data.global.x - this.deltaX < -constants.SIZE
					&& currentpiece.x > 0
					&& GameBoardController.checkCollision(currentpiece.x - 1, currentpiece.y)) {
				currentpiece.x -= 1;
				currentpiece.update();
				this.deltaX = event.data.global.x;
			}
			if(event.data.global.y - this.deltaY > constants.SIZE){
				if(currentpiece.y >= constants.HEIGHT){

				} else if(GameBoardController.checkCollision(currentpiece.x, currentpiece.y + 1)){
					currentpiece.y += 1;
					currentpiece.update();
				}
				this.deltaY = event.data.global.y;
			}
		}

		instance.onDrag = function(event) {
		}

		instance.onRelease = function(event) {
			this.deltaY = 0;
		}
		instance.onKeyDown = function(event) {
			switch(event.key) {
				case "ArrowLeft":
					if(currentpiece.x > 0
							&& GameBoardController.checkCollision(currentpiece.x - 1, currentpiece.y)) {
						currentpiece.x -= 1;
						currentpiece.update();
					}
					break;
				case "ArrowRight":
					if(currentpiece.x < constants.WIDTH  - 1
							&& GameBoardController.checkCollision(currentpiece.x + 1, currentpiece.y)) {
						currentpiece.x += 1;
						currentpiece.update();
					}
					break;
				case "ArrowUp":
					break;
				case "ArrowDown":
					this.update();
					break;
			}
		}
		instance.update = function() {
			if(GameBoardController.checkGameOver(currentpiece.x, currentpiece.y)) {
				state = states.GAMEOVER;
			}
			else if(currentpiece.y < constants.HEIGHT  - 1
						&& GameBoardController.checkCollision(currentpiece.x, currentpiece.y + 1)) {
					currentpiece.y++;
					currentpiece.update();
			} else {
				GameBoardController.insert(currentpiece);
				currentpiece.reset();
			}
			tick = 0;
			console.log(currentpiece.x + ", " + currentpiece.y);
		}
		return instance;
})();
