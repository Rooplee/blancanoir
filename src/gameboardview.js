var GameBoardView = (function () {
        var instance = {};
        var gameboard = [];
        instance.init = function() {
                for(var i = 0; i <= constants.WIDTH; i++){
                        gameboard[i] = [];
                        for(var j = 0; j < constants.HEIGHT; j++) {
                                gameboard[i][j] = -1;
                        }
                }
        }
        instance.insert = function(piece){
                gameboard[piece.x][piece.y] = new Piece(piece);
                gameContainer.addChild(gameboard[piece.x][piece.y].image);
        }
        instance.setPiece = function(x, y, color) {
                gameboard[x][y].setImage(color);
        }
        return instance;
})();
